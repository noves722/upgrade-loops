
/**********************Iteración #1: Usa includes***************************/

const products = ['Camiseta de Pokemon', 'Pantalón coquinero', 'Gorra de gansta', 'Camiseta de Basket', 'Cinrurón de Orión', 'AC/DC Camiseta']
for (let i = 0; i < products.length; i++) {
    if (products[i].includes('Camiseta')) {
        console.log(products[i])       
    }
}
/**********************Iteración #2: Condicionales avanzados***************************/

/*Comprueba en cada uno de los usuarios que tenga al menos dos trimestres aprobados
 y añade la propiedad isApproved a true o false en consecuencia. 
 Una vez lo tengas compruébalo con un console.log.*/
const alumns = [
        {name: 'Pepe Viruela', T1: false, T2: false, T3: true}, 
		{name: 'Lucia Aranda', T1: true, T2: false, T3: true},
		{name: 'Juan Miranda', T1: false, T2: true, T3: true},
		{name: 'Alfredo Blanco', T1: false, T2: false, T3: false},
		{name: 'Raquel Benito', T1: true, T2: true, T3: true}
]

for (let i = 0; i < alumns.length; i++) {
    let alumn=alumns[i];
    if ((alumn.T1&&alumn.T2)||(alumn.T2&&alumn.T3)||(alumn.T1&&alumn.T3)) {  
        alumns[i].isAproved=true;    
    } else {
        alumns[i].isAproved=false;
    }
}
console.log(alumns);
/**********************Iteración #1: Usa includes***************************/
/**********************Iteración #1: Usa includes***************************/
/**********************Iteración #1: Usa includes***************************/
/**********************Iteración #1: Usa includes***************************/
/**********************Iteración #1: Usa includes***************************/